var app = angular.module("weatherApp", []);


app.directive("controlpanel", function(){
	return {
		restrict: 'E',
		templateUrl: 'templates/controlpanel.html' 
	}
});

app.directive('weatherbox', function(){
	return{
		restrict: 'E',
		templateUrl: 'templates/weatherbox.html'
	}
});

var services = angular.module('app.services', []);

services.factory( 'Weather', function($http) {
    var Weather = function(data) { angular.extend(this, data); };

    Weather.get = function(weather) {
        return $http.get('/weather/' + weather.weatherid).then(function(response) { return response.data; });
    };

    Weather.list = function() {
        return $http.get('weather.json').then(function(response, status) { return response.data; })
    };

    return Weather;
});
